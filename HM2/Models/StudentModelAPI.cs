﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace HM2.Models
{
    public class StudentModelAPI
    {
        private int id;
        private string name;
        private string email;
        private string hobby;
        private int groupNr;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Email { get => email; set => email = value; }
        public string Hobby { get => hobby; set => hobby = value; }
        public int GroupNr { get => groupNr; set => groupNr = value; }

    }
}
