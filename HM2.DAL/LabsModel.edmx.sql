
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/08/2018 18:02:09
-- Generated from EDMX file: D:\An3Sem2\SD\Assignment 2\HM2.DAL\LabsModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Laboratories];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Assignments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Assignments];
GO
IF OBJECT_ID(N'[dbo].[Attendance]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Attendance];
GO
IF OBJECT_ID(N'[dbo].[Laboratories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Laboratories];
GO
IF OBJECT_ID(N'[dbo].[Students]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Students];
GO
IF OBJECT_ID(N'[dbo].[Submission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Submission];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Assignments'
CREATE TABLE [dbo].[Assignments] (
    [id] int  NOT NULL,
    [name] nchar(20)  NOT NULL,
    [deadline] datetime  NOT NULL,
    [description] nchar(100)  NOT NULL,
    [grade] int  NULL,
    [labId] int  NOT NULL
);
GO

-- Creating table 'Attendances'
CREATE TABLE [dbo].[Attendances] (
    [id] int  NOT NULL,
    [labId] int  NOT NULL,
    [studentId] int  NOT NULL
);
GO

-- Creating table 'Laboratories'
CREATE TABLE [dbo].[Laboratories] (
    [id] int  NOT NULL,
    [number] int  NULL,
    [date] nchar(10)  NULL,
    [curricula] varchar(70)  NULL,
    [description] nchar(60)  NULL
);
GO

-- Creating table 'Students'
CREATE TABLE [dbo].[Students] (
    [id] int  NOT NULL,
    [token] nchar(100)  NULL,
    [email] nchar(100)  NOT NULL,
    [name] nchar(20)  NOT NULL,
    [grupa] nchar(20)  NOT NULL,
    [hobby] nchar(20)  NOT NULL
);
GO

-- Creating table 'Submissions'
CREATE TABLE [dbo].[Submissions] (
    [id] int  NOT NULL,
    [assignmentId] int  NOT NULL,
    [git] nchar(60)  NOT NULL,
    [shortRemark] nchar(20)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [id] int  NOT NULL,
    [permission] nchar(20)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'Assignments'
ALTER TABLE [dbo].[Assignments]
ADD CONSTRAINT [PK_Assignments]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [PK_Attendances]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Laboratories'
ALTER TABLE [dbo].[Laboratories]
ADD CONSTRAINT [PK_Laboratories]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Students'
ALTER TABLE [dbo].[Students]
ADD CONSTRAINT [PK_Students]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Submissions'
ALTER TABLE [dbo].[Submissions]
ADD CONSTRAINT [PK_Submissions]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------