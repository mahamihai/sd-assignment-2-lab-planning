//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HM2.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Submission
    {
        public int id { get; set; }
        public int assignmentId { get; set; }
        public string git { get; set; }
        public string shortRemark { get; set; }
        public int tries { get; set; }
        public int studentId { get; set; }
        public Nullable<int> grade { get; set; }
    
        public virtual Assignment Assignment { get; set; }
        public virtual Student Student { get; set; }
    }
}
