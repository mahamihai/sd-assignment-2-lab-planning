
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/09/2018 11:09:20
-- Generated from EDMX file: F:\An3Sem2\SD\Assignment 2\HM2\HM2.DAL\AdoModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Laboratories];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Assignments_Assignments]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Assignments] DROP CONSTRAINT [FK_Assignments_Assignments];
GO
IF OBJECT_ID(N'[dbo].[FK_Attendance_Students]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attendances] DROP CONSTRAINT [FK_Attendance_Students];
GO
IF OBJECT_ID(N'[dbo].[FK_Attendances_Laboratories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Attendances] DROP CONSTRAINT [FK_Attendances_Laboratories];
GO
IF OBJECT_ID(N'[dbo].[FK_Students_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Students] DROP CONSTRAINT [FK_Students_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_Submissions_Assignments]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Submissions] DROP CONSTRAINT [FK_Submissions_Assignments];
GO
IF OBJECT_ID(N'[dbo].[FK_Submissions_Students]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Submissions] DROP CONSTRAINT [FK_Submissions_Students];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Assignments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Assignments];
GO
IF OBJECT_ID(N'[dbo].[Attendances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Attendances];
GO
IF OBJECT_ID(N'[dbo].[Laboratories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Laboratories];
GO
IF OBJECT_ID(N'[dbo].[Students]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Students];
GO
IF OBJECT_ID(N'[dbo].[Submissions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Submissions];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Assignments'
CREATE TABLE [dbo].[Assignments] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] varchar(60)  NOT NULL,
    [deadline] datetime  NOT NULL,
    [description] varchar(60)  NOT NULL,
    [labId] int  NOT NULL
);
GO

-- Creating table 'Attendances'
CREATE TABLE [dbo].[Attendances] (
    [id] int IDENTITY(1,1) NOT NULL,
    [labId] int  NOT NULL,
    [studentId] int  NOT NULL
);
GO

-- Creating table 'Laboratories'
CREATE TABLE [dbo].[Laboratories] (
    [id] int IDENTITY(1,1) NOT NULL,
    [number] int  NOT NULL,
    [date] datetime  NULL,
    [curricula] varchar(60)  NULL,
    [description] varchar(60)  NULL,
    [classId] int  NOT NULL
);
GO

-- Creating table 'Students'
CREATE TABLE [dbo].[Students] (
    [id] int  NOT NULL,
    [token] varchar(100)  NOT NULL,
    [email] varchar(100)  NULL,
    [name] varchar(100)  NULL,
    [grupa] varchar(100)  NULL,
    [hobby] varchar(100)  NULL
);
GO

-- Creating table 'Submissions'
CREATE TABLE [dbo].[Submissions] (
    [id] int IDENTITY(1,1) NOT NULL,
    [assignmentId] int  NOT NULL,
    [git] varchar(60)  NOT NULL,
    [shortRemark] varchar(60)  NOT NULL,
    [tries] int  NOT NULL,
    [studentId] int  NULL,
    [grade] int  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [id] int IDENTITY(1,1) NOT NULL,
    [permission] varchar(100)  NOT NULL,
    [username] varchar(100)  NULL,
    [password] varchar(100)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'Assignments'
ALTER TABLE [dbo].[Assignments]
ADD CONSTRAINT [PK_Assignments]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [PK_Attendances]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Laboratories'
ALTER TABLE [dbo].[Laboratories]
ADD CONSTRAINT [PK_Laboratories]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Students'
ALTER TABLE [dbo].[Students]
ADD CONSTRAINT [PK_Students]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Submissions'
ALTER TABLE [dbo].[Submissions]
ADD CONSTRAINT [PK_Submissions]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [labId] in table 'Assignments'
ALTER TABLE [dbo].[Assignments]
ADD CONSTRAINT [FK_Assignments_Assignments]
    FOREIGN KEY ([labId])
    REFERENCES [dbo].[Laboratories]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Assignments_Assignments'
CREATE INDEX [IX_FK_Assignments_Assignments]
ON [dbo].[Assignments]
    ([labId]);
GO

-- Creating foreign key on [assignmentId] in table 'Submissions'
ALTER TABLE [dbo].[Submissions]
ADD CONSTRAINT [FK_Submissions_Assignments]
    FOREIGN KEY ([assignmentId])
    REFERENCES [dbo].[Assignments]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Submissions_Assignments'
CREATE INDEX [IX_FK_Submissions_Assignments]
ON [dbo].[Submissions]
    ([assignmentId]);
GO

-- Creating foreign key on [studentId] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [FK_Attendance_Students]
    FOREIGN KEY ([studentId])
    REFERENCES [dbo].[Students]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Attendance_Students'
CREATE INDEX [IX_FK_Attendance_Students]
ON [dbo].[Attendances]
    ([studentId]);
GO

-- Creating foreign key on [labId] in table 'Attendances'
ALTER TABLE [dbo].[Attendances]
ADD CONSTRAINT [FK_Attendances_Laboratories]
    FOREIGN KEY ([labId])
    REFERENCES [dbo].[Laboratories]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Attendances_Laboratories'
CREATE INDEX [IX_FK_Attendances_Laboratories]
ON [dbo].[Attendances]
    ([labId]);
GO

-- Creating foreign key on [id] in table 'Students'
ALTER TABLE [dbo].[Students]
ADD CONSTRAINT [FK_Students_Users]
    FOREIGN KEY ([id])
    REFERENCES [dbo].[Users]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [studentId] in table 'Submissions'
ALTER TABLE [dbo].[Submissions]
ADD CONSTRAINT [FK_Submissions_Students]
    FOREIGN KEY ([studentId])
    REFERENCES [dbo].[Students]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Submissions_Students'
CREATE INDEX [IX_FK_Submissions_Students]
ON [dbo].[Submissions]
    ([studentId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------