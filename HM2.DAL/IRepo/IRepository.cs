﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM2.DAL.IRepo
{
    public interface IRepository<T> where T : class
    {

       
        void Add(T entity);
        List<T> getAll();
        void UpdateById(T entity,int id);
        void Insert(T entity);
        void UpdateById(T entity,object id);
        void DeleteById(object id);
    }
}
