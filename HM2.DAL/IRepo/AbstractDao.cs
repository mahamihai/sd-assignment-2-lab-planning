﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2;
using EntityState = System.Data.Entity.EntityState;

namespace HM2.DAL.IRepo
{
    public abstract class AbstractDao<T, C>:IRepository<C> where C : class where T : DbContext, new()
    {

        private T _entities = new T();

        public T Context
        {

            get { return _entities; }
            set { _entities = value; }
        }
      

        public void Add(C entity)
        {
           
            _entities.Set<C>().Add(entity);
            _entities.Entry(entity).State = EntityState.Added;
            var t = _entities.Set<C>().ToList();
            this.Save();
        }
       public List<C> getAll()
        {
           return  _entities.Set<C>().ToList();
        }
        public void UpdateById(C entity,int id)

        {
            var toBeUpdated = _entities.Set<C>().Find(id);
            _entities.Entry(toBeUpdated).CurrentValues.SetValues(entity);
            _entities.SaveChanges();
        }

        public void Save()
        {
            try
            {
                _entities.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var t = e.ToString();
            }
            catch (Exception e)
            {
                var t = e.ToString();
            }
        }

        public void Insert(C entity)
        {
            _entities.Set<C>().Add(entity);
            this.Save();
        }

        public void UpdateById(C entity, object id)
        {
            var t = _entities.Set<C>().Find(id);
            _entities.Entry(t).CurrentValues.SetValues(entity);
            this.Save();
        }
        public  void DeleteById(object id)
       {

           try
           {

               var result = this._entities.Set<C>().Find(id);
               this._entities.Set<C>().Attach(result);
              
               this._entities.Set<C>().Remove(result);
               this._entities.SaveChanges();
                 //  _entities.Set<C>().Attach()
                 //  _entities.Set<C>().Remove(result);

                 this.Save();
           }
           catch (EntityException e)
           {
               var t = e.ToString();
           }
           catch (Exception e)
           {
               var t = e.ToString();
           }
        }
    }
}
