USE [master]
GO
/****** Object:  Database [Laboratories]    Script Date: 16-Apr-18 5:41:52 PM ******/
CREATE DATABASE [Laboratories]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Laboratories', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Laboratories.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Laboratories_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Laboratories_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Laboratories] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Laboratories].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Laboratories] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Laboratories] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Laboratories] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Laboratories] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Laboratories] SET ARITHABORT OFF 
GO
ALTER DATABASE [Laboratories] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Laboratories] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Laboratories] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Laboratories] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Laboratories] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Laboratories] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Laboratories] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Laboratories] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Laboratories] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Laboratories] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Laboratories] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Laboratories] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Laboratories] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Laboratories] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Laboratories] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Laboratories] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Laboratories] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Laboratories] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Laboratories] SET  MULTI_USER 
GO
ALTER DATABASE [Laboratories] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Laboratories] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Laboratories] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Laboratories] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Laboratories] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Laboratories] SET QUERY_STORE = OFF
GO
USE [Laboratories]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Laboratories]
GO
/****** Object:  Table [dbo].[Assignments]    Script Date: 16-Apr-18 5:41:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assignments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nchar](20) NOT NULL,
	[deadline] [date] NOT NULL,
	[description] [nchar](100) NOT NULL,
	[grade] [int] NULL,
	[labId] [int] NOT NULL,
 CONSTRAINT [PK_Assignments] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attendance]    Script Date: 16-Apr-18 5:41:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attendance](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[labId] [int] NOT NULL,
	[studentId] [int] NOT NULL,
 CONSTRAINT [PK_Attendance] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Laboratories]    Script Date: 16-Apr-18 5:41:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Laboratories](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number] [int] NULL,
	[date] [nchar](10) NULL,
	[curricula] [varchar](70) NULL,
	[description] [nchar](60) NULL,
 CONSTRAINT [PK_Laboratories] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Students]    Script Date: 16-Apr-18 5:41:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[token] [nchar](100) NOT NULL,
	[email] [nchar](100) NULL,
	[name] [nchar](20) NULL,
	[grupa] [nchar](20) NULL,
	[hobby] [nchar](20) NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Submission]    Script Date: 16-Apr-18 5:41:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Submission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[assignmentId] [int] NOT NULL,
	[git] [nchar](60) NOT NULL,
	[shortRemark] [nchar](20) NOT NULL,
 CONSTRAINT [PK_Submission] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 16-Apr-18 5:41:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[permission] [nchar](20) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assignments] ON 

INSERT [dbo].[Assignments] ([id], [name], [deadline], [description], [grade], [labId]) VALUES (1, N'das                 ', CAST(N'2018-02-24' AS Date), N'fd                                                                                                  ', 1, 4)
SET IDENTITY_INSERT [dbo].[Assignments] OFF
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([id], [token], [email], [name], [grupa], [hobby]) VALUES (2, N'123                                                                                                 ', NULL, N'Peter Cercetasul    ', N'B3                  ', N'maraton si Marathon ')
INSERT [dbo].[Students] ([id], [token], [email], [name], [grupa], [hobby]) VALUES (4, N'12                                                                                                  ', NULL, N'Dan cercetatorul    ', N'das                 ', N'beciu               ')
SET IDENTITY_INSERT [dbo].[Students] OFF
SET IDENTITY_INSERT [dbo].[Submission] ON 

INSERT [dbo].[Submission] ([id], [assignmentId], [git], [shortRemark]) VALUES (1, 1, N'dfsfs                                                       ', N'fdfs                ')
SET IDENTITY_INSERT [dbo].[Submission] OFF
ALTER TABLE [dbo].[Submission]  WITH CHECK ADD  CONSTRAINT [FK_Submission_Submission] FOREIGN KEY([assignmentId])
REFERENCES [dbo].[Assignments] ([id])
GO
ALTER TABLE [dbo].[Submission] CHECK CONSTRAINT [FK_Submission_Submission]
GO
USE [master]
GO
ALTER DATABASE [Laboratories] SET  READ_WRITE 
GO
