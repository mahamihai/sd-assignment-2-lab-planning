﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HM2.WEB.Controllers
{
    public  class MyMapper
    {
        public static AutoMapper.IMapper setupMapper<T, U>()
        {
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
    }
}