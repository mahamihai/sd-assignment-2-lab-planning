﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.WEB.Models;

namespace HM2.WEB.Controllers
{
    public class AssignmentController : ApiController
    {
        private IAssignment _assignmentServices;

        public AssignmentController(IAssignment _assignmentServices)
        {
            this._assignmentServices = _assignmentServices;

        }
        [Route("Assignment/")]
        [HttpGet]
        [System.Web.Http.Authorize(Roles = "teacher, Student")]
        public IEnumerable<AssignmentModel> GetAssignments()
        {
            return this._assignmentServices.getAssignments();

        }
        [Route("Assignment/{labid}")]
        [HttpGet]
        [Authorize(Roles="teacher,Student")]
        public IEnumerable<AssignmentModel> GetAssignementsByLabId(int labid)
        {
            return this._assignmentServices.getAssignmentsByLab(labid);

        }

        [Route("Assignment/")]
        [HttpPost]
        [Authorize(Roles = "teacher")]
        public string PostAss([FromBody]AssignmentAPI lab)
        {
            var convertedLab = MyMapper.setupMapper<AssignmentAPI, AssignmentModel>().Map<AssignmentModel>(lab);
            return this._assignmentServices.addAssignment(convertedLab);
        }
        //PuT METHODS

        [Route("Assignment/")]
        [HttpPut]
        [Authorize(Roles = "teacher")]
        public string PutAssi([FromBody]AssignmentModel value)
        {
            return this._assignmentServices.updateAssignment(value);
        }
        //DELETE METHODS
        [Route("Assignment/{id}")]
        [HttpDelete]
        [Authorize(Roles = "Teacher")]
        // DELETE: api/Teacher/5
        public string DeleteAssi(int id)
        {
            return this._assignmentServices.deleteAssignemnt(id);
        }



    }
}
