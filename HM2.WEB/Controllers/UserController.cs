﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.WEB.Models;
using WebGrease.Css.Ast.Selectors;

namespace HM2.WEB.Controllers
{
    public class UserController : ApiController
    {
        public UserController(IUser _userRepo,IStudent _studentRepo)
        {
            this._userRepo = _userRepo;
            this._studentRepo = _studentRepo;
        }

        private IUser _userRepo;
        private IStudent _studentRepo;

        [Route("User/login")]
        [HttpPost]
        public UserModel Login([FromUri]string username, [FromUri]string password)
        {


            return this._userRepo.checkUser(username, password);


        }
        [Route("User/test")]
        [HttpPost]
        public string Login([FromUri]string ok)
        {


            return ok;


        }

        [HttpPut]
        [Route("User/register")]
        public bool Register([FromBody]UserInfoApi studentApi)
        {
            try
            {
                //var student = MyMapper.setupMapper<UserAPI, StudentModel>().Map<StudentModel>(studentApi);
                var user = MyMapper.setupMapper<UserInfoApi, UserModel>().Map<UserModel>(studentApi);
                var student = MyMapper.setupMapper<UserInfoApi, StudentModel>().Map<StudentModel>(studentApi);
               
                this._userRepo.registerUser(user, student);
                this._studentRepo.updateStudent(student);

                return true;
            }
            catch
            {
                return false;
            }

        }

    }
}
