﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.WEB.Models;

namespace HM2.WEB.Controllers
{
    public class LaboratoryController : ApiController
    {
        public LaboratoryController(ILab _labService)
        {
            this._labService = _labService;
        }

        private ILab _labService;


        [Route("Laboratory/")]
        [HttpGet]
        [Authorize(Roles = "teacher,Student")]
        public IEnumerable<LaboratoryModel> GetLabs()
        {
            return this._labService.getLaboratories();

        }
        [Route("Laboratory/{id}")]
        [HttpGet]
        [Authorize(Roles = "teacher,Student")]

        public IEnumerable<LaboratoryModel> GetLabsFilteres(string id)
        {
            return this._labService.getLabsFiltered(id);

        }
        [Route("Laboratory/")]
        [HttpPost]
        [Authorize(Roles = "teacher")]

        public string PostLaboratory([FromBody]LaboratoryAPI lab)
        {
            var convertedLab = MyMapper.setupMapper<LaboratoryAPI, LaboratoryModel>().Map<LaboratoryModel>(lab);
            return this._labService.addLaboratory(convertedLab);
        }
        [Route("Laboratory/")]
        [HttpPut]
        [Authorize(Roles = "teacher")]
        public string PutLab([FromBody]LaboratoryModel value)
        {
            return this._labService.updateLaboratory(value);
        }
        [Route("Laboratory/")]
        [HttpDelete]
        [Authorize(Roles = "teacher")]
        // DELETE: api/Teacher/5
        public string DeleteLab(int id)
        {
            return this._labService.deleteLab(id);
        }

    }
}
