﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HM2.BLL.IServices;
using HM2.BLL.Models;

namespace HM2.WEB.Controllers
{
    public class StudentController : ApiController
    {
        public StudentController(IStudent _studentServices)
        {
            this._studentServices = _studentServices;
        }

        private IStudent _studentServices;
        [Route("Student/{id}")]
        [HttpDelete]
        [Authorize(Roles = "teacher")]
        // DELETE: api/Teacher/5
        public string DeleteStudent(int id)
        {
            return this._studentServices.deleteStudent(id);
        }
        [Route("Student/")]
        [HttpPut]
        [Authorize(Roles = "Student,teacher")]
        public string PutStudent([FromBody]StudentModel value)
        {
            return this._studentServices.updateStudent(value);
        }
        [Route("Student/")]
        [HttpPost]
        [Authorize(Roles = "teacher")]
        public string PostStudent([FromBody]String email)
        {
            return this._studentServices.addStudent(email);
        }
        // POST: api/Teacher
        [Route("Student/")]
        [HttpGet]
        [Authorize(Roles = "teacher")]
        public IEnumerable<StudentModel> GetStudents()
        {
            return this._studentServices.getStudents();

        }
        [Route("LuciEOkLinku")]
        [HttpGet]
        
        public string test()
        {
            return "Hâțz Johnule";

        }

    }
}
