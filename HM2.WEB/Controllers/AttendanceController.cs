﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.WEB.Models;

namespace HM2.WEB.Controllers
{
    public class AttendanceController : ApiController
    {
        private IAttendance _attendanceServices;
        public AttendanceController(IAttendance _attendanceServices)
        {
            this._attendanceServices = _attendanceServices;
        }

        [Route("Attendance/")]
        [HttpGet]
        [Authorize(Roles = "teacher")]
        public IEnumerable<AttendanceModel> GetAttendance()
        {
            var att= this._attendanceServices.getAttendance();
            return att;

        }
     
        //POST METHODS!!!!

        // POST: api/Teacher

        [Route("Attendance/")]
        [HttpPost]
        [Authorize(Roles = "teacher")]
        public string PostLaboratory([FromBody]AttendanceAPI lab)
        {
            var convertedLab = MyMapper.setupMapper<AttendanceAPI, AttendanceModel>().Map<AttendanceModel>(lab);
            return this._attendanceServices.addAttendance(convertedLab);
        }
        //PuT METHODS


        [Route("Attendance/")]
        [HttpPut]
        [Authorize(Roles = "teacher")]
        public string PutAttendance([FromBody]AttendanceModel value)
        {
            return this._attendanceServices.updateAttendance(value);
        }
        //DELETE METHODS


        //DELETE METHODS
        [Route("Attendance/{id}")]
        [HttpDelete]
        [Authorize(Roles = "teacher")]
        // DELETE: api/Teacher/5
        public string DeleteAttendance(int id)
        {
            return this._attendanceServices.deleteAttendance(id);
        }
    }
}
