﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.WEB.Models;

namespace HM2.WEB.Controllers
{
    public class SubmissionController : ApiController
    {
        public SubmissionController(ISubmission _submissionServices)
        {
            this._submissionServices = _submissionServices;
        }
        private ISubmission _submissionServices;

        [Route("Submission/")]
        [HttpGet]
        [Authorize(Roles = "teacher")]
        public IEnumerable<SubmissionModel> GetSubbmissions()
        {
            return this._submissionServices.getSumbissions();

        }
        [Route("Submission/{assiId}")]
        [HttpGet]
        [Authorize(Roles = "teacher")]
        public IEnumerable<SubmissionModel> GetSubbmissionsByAssignmentId(int assiId)
        {
            return this._submissionServices.getByAssignmentId(assiId);

        }
        //POST METHODS!!!!

        // POST: api/Teacher

        [Route("Submission/")]
        [HttpPost]
        [Authorize(Roles = "Student,teacher")]
        public string PostSubmission([FromBody]SubmissionAPI lab)
        {
            var convertedLab = MyMapper.setupMapper<SubmissionAPI, SubmissionModel>().Map<SubmissionModel>(lab);
            return this._submissionServices.addSubmission(convertedLab);
        }
        //PuT METHODS


        [Route("Submission/")]
        [HttpPut]
        [Authorize(Roles = "teacher")]

        public string PutAttendance([FromBody]SubmissionModel value)
        {
            return this._submissionServices.updateSubmission(value);
        }
        //DELETE METHODS


        //DELETE METHODS
        [Route("Submission/{id}")]
        [HttpDelete]
        // DELETE: api/Teacher/5
        [Authorize(Roles = "teacher")]

        public string DeleteAttendance(int id)
        {
            return this._submissionServices.deleteSubmission(id);
        }
    }
}
