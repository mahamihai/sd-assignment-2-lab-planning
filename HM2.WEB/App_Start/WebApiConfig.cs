﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Unity;
using HM2.BLL.Models;
using HM2.BLL.IServices;
using HM2.BLL.Services;
using HM2.DAL;
using HM2.DAL.IRepo;
using HM2.DAL.Repo;
using HM2.WEB.App_Start;
using LibraryApplicationWithAuth.Handlers;
using Unity.Lifetime;

namespace HM2.WEB
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.MessageHandlers.Add(new AuthenticationHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            Mapper.Initialize(cfg => {
               
            });
            var unityContainer = new Unity.UnityContainer();

            // Register IGame so when dependecy is detected
            // it provides a TrivialPursuit instance
           
           // unityContainer.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            unityContainer
                .RegisterType<IRepository<Laboratory>, LaboratoryRepo>(new HierarchicalLifetimeManager())
                .RegisterType<IRepository<Assignment>, AssignmentRepo>(new HierarchicalLifetimeManager())
                .RegisterType<IRepository<Submission>, SubmissionRepo>(new HierarchicalLifetimeManager())
                .RegisterType<IRepository<Student>, StudentRepo>(new HierarchicalLifetimeManager())
                .RegisterType<IRepository<User>, UserRepo>(new HierarchicalLifetimeManager())
                .RegisterType<IRepository<Attendance>, AttendanceRepo>(new HierarchicalLifetimeManager())
                .RegisterType<IStudent, StudentServices>(new HierarchicalLifetimeManager())
                .RegisterType<IAttendance, AttendanceServices>(new HierarchicalLifetimeManager())
                .RegisterType<ILab, LaboratoryServices>(new HierarchicalLifetimeManager())
                .RegisterType<IUser, UserServices>(new HierarchicalLifetimeManager())
                .RegisterType<ISubmission, SubmissionServices>(new HierarchicalLifetimeManager())
                .RegisterType<IAssignment, AssignmentServices>(new HierarchicalLifetimeManager())

                .RegisterType<ILab, LaboratoryServices>(new HierarchicalLifetimeManager());
           

            
            config.DependencyResolver = new UnityResolver(unityContainer);
            // Instance a Table class object through Unity
           

        }
    }
}
