﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Security.Principal;
using HM2.BLL.IServices;
using HM2.BLL.Services;


namespace LibraryApplicationWithAuth.Handlers
{
    public class AuthenticationHandler : DelegatingHandler
    {
        private readonly IUser credentialCheckerService;
        public AuthenticationHandler()
        {
            this.credentialCheckerService = new UserServices();

        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                if (request.Headers.Contains("Authorization"))
                {
                    HttpContext httpContext = HttpContext.Current;
                    string authHeader = httpContext.Request.Headers["Authorization"];

                    string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();

                    Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                    string tokens = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                    if (tokens != null)
                    {
                        string[] tokensValues = tokens.Split(':');

                        var ObjUser = credentialCheckerService.checkUser(tokensValues[0], tokensValues[1]);
                        if (ObjUser != null)
                        {
                            IPrincipal principal = new GenericPrincipal(new GenericIdentity(ObjUser.username), ObjUser.permission.Split(','));
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                        }
                        else
                        {
                            //The user is unauthorize and return 401 status  
                            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                            var tsc = new TaskCompletionSource<HttpResponseMessage>();
                            tsc.SetResult(response);
                            return tsc.Task;
                        }
                    }
                    else
                    {
                        //Bad Request request because Authentication header is set but value is null  
                        var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                        var tsc = new TaskCompletionSource<HttpResponseMessage>();
                        tsc.SetResult(response);
                        return tsc.Task;
                    }
                }
                return base.SendAsync(request, cancellationToken);
            }
            catch(Exception e)
            {
                //User did not set Authentication header  
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(response);
                return tsc.Task;
            }
        }

    }
}