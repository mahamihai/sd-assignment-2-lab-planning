﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.DAL;
using HM2.DAL.IRepo;

namespace HM2.BLL.Services
{
   public class AssignmentServices:IAssignment
    {
        private IRepository<Assignment> _assignmentRepo;

        public AssignmentServices(IRepository<Assignment> _assignmentRepo)
        {
            this._assignmentRepo = _assignmentRepo;
        }


       public List<AssignmentModel> getAssignments()
        {
            var assignments = _assignmentRepo.getAll().Select(x=>this.setupMapper<Assignment,AssignmentModel>().Map<AssignmentModel>(x)).ToList();
            return assignments;
        }
        public AutoMapper.IMapper setupMapper<T, U>()
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
        public List<AssignmentModel> getAssignmentsByLab(int labid)
        {
            var assignments = _assignmentRepo.getAll().Select(x => this.setupMapper<Assignment, AssignmentModel>().Map<AssignmentModel>(x)).Where(x=>x.labId==labid).ToList();
            return assignments;
        }
       
        public string addAssignment(AssignmentModel assi)
        {
            try
            {
                var assignment = this.setupMapper<AssignmentModel, Assignment>().Map<Assignment>(assi);
                this._assignmentRepo.Add(assignment);
                return "Success";
            }
            catch
            {
                return "Error";
            }
        }

        public string updateAssignment(AssignmentModel assi)
        {
            try
            {
                var converted = this.setupMapper<AssignmentModel, Assignment>().Map<Assignment>(assi);
                this._assignmentRepo.UpdateById(converted, converted.id);
                return "Success";
            }
            catch
            {
                return "Failed";
            }
        }

        public string deleteAssignemnt(object id)
        {
            try
            {
                this._assignmentRepo.DeleteById(id);

                return "Success";
            }
            catch
            {
                return "Failed";
            }

        }
    }
}
