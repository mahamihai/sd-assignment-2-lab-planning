﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.DAL;
using HM2.DAL.IRepo;

namespace HM2.BLL.Services
{
   public class StudentServices:IStudent
    {
        private IRepository<Student> _studentRepo;
        private IRepository<User> _userRepo;
        public StudentServices(IRepository<Student> _studentRepo, IRepository<User> _userRepo)
        {
            this._studentRepo = _studentRepo;
            this._userRepo = _userRepo;
        }
        public AutoMapper.IMapper setupMapper<T, U>()
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
        public List<StudentModel> getStudents()
        {

            var students = _studentRepo.getAll();
            return students.Select(x => setupMapper<Student, StudentModel>().Map<StudentModel>(x)).ToList();
        }
        public string addStudent(String email)
        {
            try
            {
                User newUser = new User();
                newUser.permission = "student";
                Student newStudent = new Student();
                newStudent.email = email;
                newStudent.token = Guid.NewGuid().ToString("n").Substring(0, 8);
                newUser.Student = newStudent;
                this._userRepo.Add(newUser);
                EmailFactory fact=new EmailFactory();
                fact.sendEmail(newStudent.email.Trim(),newStudent.token);
                return "Success";
            }
            catch(Exception e)
            {
                var t = e.ToString();
                return "Error";
            }

        }



        public string deleteStudent(object id)
        {
            try
            {
               // this._studentRepo.DeleteById(id);
                this._userRepo.DeleteById(id);
                return "Success";
            }
            catch(Exception e)
            {
                var t = e.ToString();
                return "Failed";
            }

        }
        public string updateStudent(StudentModel student)
        {
            try
            {
                var converted = this.setupMapper<StudentModel, Student>().Map<Student>(student);
                this._studentRepo.UpdateById(converted, converted.id);
                return "Success";
            }
            catch(Exception e)
            {
                var t = e.ToString();
                return "Failed";
            }
        }


    }
}
