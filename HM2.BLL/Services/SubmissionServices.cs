﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.DAL;
using HM2.DAL.IRepo;

namespace HM2.BLL.Services
{
   public class SubmissionServices:ISubmission
    {
        public AutoMapper.IMapper setupMapper<T, U>()
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
        private IRepository<Submission> _submissionRepo;
        public SubmissionServices(IRepository<Submission> _submissionRepo)
        {
            this._submissionRepo = _submissionRepo;
        }

   


        public List<SubmissionModel> getSumbissions()
        {
            var labs = this._submissionRepo.getAll().Select(x => this.setupMapper<Submission, SubmissionModel>().Map<SubmissionModel>(x)).ToList();
            return labs;
        }
        public string addSubmission(SubmissionModel assign)
        {
            try
            {
                var previous = this._submissionRepo.getAll()
                    .SingleOrDefault(x => x.assignmentId == assign.assignmentId);
                if (previous != null)
                {
                    if (previous.tries > 0)
                    {
                        previous.git = assign.git;
                        previous.shortRemark = assign.shortRemark;
                        previous.tries--;
                        this._submissionRepo.UpdateById(previous, previous.id);
                        return "Success,updated";
                    }
                    else
                    {
                        return "That was a last chance";
                    }

                }
                else
                {
                    var submission = this.setupMapper<SubmissionModel, Submission>().Map<Submission>(assign);

                    submission.tries = 3;
                    this._submissionRepo.Insert(submission);
                    return "Succes,first try";
                }
            }
            catch
            {
                return "Failed";
            }


        }
        public string updateSubmission(SubmissionModel sub)
        {
            try
            {
                var converted = this.setupMapper<SubmissionModel, Submission>().Map<Submission>(sub);
                this._submissionRepo.UpdateById(converted, sub.id);
                return "Success";
            }
            catch(Exception e)
            {
                var t = e.ToString();
                return "Failed";
            }
        }

        public List<SubmissionModel> getByAssignmentId(int id)
        {
            var all = this._submissionRepo.getAll().Where(x => x.assignmentId == id).Select(x=>this.setupMapper<Submission,SubmissionModel>().Map<SubmissionModel>(x)).ToList();
            return all;
        }
        public string deleteSubmission(object id)
        {
            try
            {
                this._submissionRepo.DeleteById(id);

                return "Success";
            }
            catch
            {
                return "Failed";
            }

        }

    }
}
