﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.DAL;
using HM2.DAL.IRepo;

namespace HM2.BLL.Services
{
   public  class AttendanceServices:IAttendance
    {
        private IRepository<Attendance> _attendanceRepo;
        public AttendanceServices(  IRepository<Attendance> _attendanceRepo)
        {
            this._attendanceRepo = _attendanceRepo;
        }
        public AutoMapper.IMapper setupMapper<T, U>()
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
        public List<AttendanceModel> getAttendance()
        {
            var attendance = this._attendanceRepo.getAll().Select(x => this.setupMapper<Attendance, AttendanceModel>().Map<AttendanceModel>(x)).ToList();
            return attendance;
        }

        public string addAttendance(AttendanceModel att)
        {
            try
            {
                var attendanceConverted = this.setupMapper<AttendanceModel, Attendance>().Map<Attendance>(att);
                this._attendanceRepo.Add(attendanceConverted);
                return "Success";
            }
            catch
            {
                return "Error";
            }
        }
        public string updateAttendance(AttendanceModel student)
        {
            try
            {
                var converted = this.setupMapper<AttendanceModel, Attendance>().Map<Attendance>(student);
                this._attendanceRepo.UpdateById(converted, converted.id);
                return "Success";
            }
            catch
            {
                return "Failed";
            }
        }
        public string deleteAttendance(object id)
        {
            try
            {
                this._attendanceRepo.DeleteById(id);

                return "Success";
            }
            catch
            {
                return "Failed";
            }

        }
    }
}
