﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.DAL;
using HM2.DAL.IRepo;
using HM2.DAL.Repo;

namespace HM2.BLL.Services
{
     public class UserServices:IUser
    {
        public UserServices(IRepository<User> _userRepo, IRepository<Student> _studentRepo)
        {

       
            this._UserRepo = _userRepo;
            this._studentRepo = _studentRepo;
        }
        public UserServices()
        {


            this._UserRepo = new UserRepo();
            this._studentRepo = new StudentRepo();
        }


        private IRepository<User> _UserRepo;
        private IRepository<Student> _studentRepo;
        public AutoMapper.IMapper setupMapper<T, U>()
        {
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
        public UserModel checkUser(string username, string password)
        {

            username = username.Trim();
            password = password.Trim();
            var all = this._UserRepo.getAll();
            var user = all.Where(x =>(x.password!=null)
            
            
            && (x.username!=null) &&
                                     x.password.Trim().Equals(password.Trim()) && x.username.Trim().Equals(username.Trim())).FirstOrDefault();

            if (user != null)
            {
                var converted = this.setupMapper<User, UserModel>().Map<UserModel>(user);
                return converted;
            }
            return null;
        }


        public bool registerUser(UserModel userModel, StudentModel student)
        {


            //where token is not null and equal to the inputed one
            var students = this._studentRepo.getAll();
            var stud = students.Where(x => x.token.Trim().Equals(student.token.Trim())).FirstOrDefault();

            if (stud != null)
            {
                try
                {

                    //mapp them to db models

                    var user = this.setupMapper<UserModel, User>().Map<User>(userModel);
                    //update id

                    var studentu = this.setupMapper<StudentModel, Student>().Map<Student>(student);

                    user.id = stud.id;
                    //save to db 
                    user.permission = "student";

                    this._UserRepo.UpdateById(user, stud.id);
                    studentu.token = "";
                    studentu.id = stud.id;
                    this._studentRepo.UpdateById(studentu, stud.id);
                    return true;

                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }



        }
        public string addUser(UserModel lab)
        {
            try
            {
                var converted = this.setupMapper<UserModel, User>().Map<User>(lab);
                this._UserRepo.Add(converted);
                return "Success";
            }
            catch
            {
                return "Error";
            }
        }

    }
    }
