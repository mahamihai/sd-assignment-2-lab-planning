﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HM2.BLL.IServices;
using HM2.BLL.Models;
using HM2.DAL;
using HM2.DAL.IRepo;

namespace HM2.BLL.Services
{
   public class LaboratoryServices:ILab
    {
        public AutoMapper.IMapper setupMapper<T, U>()
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<T, U>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        }
        private IRepository<Laboratory> _laboratoryRepo;
        public LaboratoryServices(IRepository<Laboratory> _laboratoryRepo)
        {
            this._laboratoryRepo = _laboratoryRepo;
        }

        public List<LaboratoryModel> getLabsFiltered(string filter)
        {
            var laboratories = this._laboratoryRepo.getAll()
                .Where(x => x.curricula.Contains(filter) || x.description.Contains(filter));
            var filtered = laboratories
                .Select(x => this.setupMapper<Laboratory, LaboratoryModel>().Map<LaboratoryModel>(x)).ToList();
            return filtered;
        }



        public List<LaboratoryModel> getLaboratories()
        {
            var labs = this._laboratoryRepo.getAll().Select(x => this.setupMapper<Laboratory, LaboratoryModel>().Map<LaboratoryModel>(x)).ToList();
            return labs;
        }
        public string addLaboratory(LaboratoryModel labFromInput)
        {
            try
            {
                var laboratory = this.setupMapper<LaboratoryModel, Laboratory>().Map<Laboratory>(labFromInput);
                this._laboratoryRepo.Add(laboratory);
                return "Success";
            }
            catch
            {
                return "Error";
            }

        }
        public string updateLaboratory(LaboratoryModel lab)
        {
            try
            {
                var converted = this.setupMapper<LaboratoryModel, Laboratory>().Map<Laboratory>(lab);
                this._laboratoryRepo.UpdateById(converted, lab.id);
                return "Success";
            }
            catch
            {
                return "Failed";
            }
        }
        public string deleteLab(object id)
        {
            try
            {
                this._laboratoryRepo.DeleteById(id);

                return "Success";
            }
            catch
            {
                return "Failed";
            }

        }
    }
}
