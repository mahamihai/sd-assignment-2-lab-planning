﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;

namespace HM2.BLL.IServices
{
  public  interface ISubmission
    {
        List<SubmissionModel> getSumbissions();
        string addSubmission(SubmissionModel lab);
        string updateSubmission(SubmissionModel lab);
        string deleteSubmission(object id);
        List<SubmissionModel> getByAssignmentId(int id);

    }
}
