﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;

namespace HM2.BLL.IServices
{
  public  interface ServicesInterface

    {
        List<LaboratoryModel> getLabsFiltered(string filter);
        List<AssignmentModel> getAssignments(int labId);
        string submitAssignment(SubmissionModel assign);
       
       
       
    }
}
