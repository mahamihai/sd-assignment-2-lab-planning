﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;

namespace HM2.BLL.IServices
{
   public  interface IAttendance
    {
        List<AttendanceModel> getAttendance();
        string addAttendance(AttendanceModel att);
        string updateAttendance(AttendanceModel student);
        string deleteAttendance(object id);
    }
}
