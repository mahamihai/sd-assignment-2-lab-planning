﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;

namespace HM2.BLL.IServices
{
  public   interface IAssignment
    {
        List<AssignmentModel> getAssignments();
        string addAssignment(AssignmentModel assi);
        string updateAssignment(AssignmentModel assi);
        string deleteAssignemnt(object id);
        List<AssignmentModel> getAssignmentsByLab(int labid);
    }
}
