﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;

namespace HM2.BLL.IServices
{
   public interface IStudent
   {
       List<StudentModel> getStudents();
       string addStudent(String email);
       string deleteStudent(object id);
       string updateStudent(StudentModel student);



    }
}
