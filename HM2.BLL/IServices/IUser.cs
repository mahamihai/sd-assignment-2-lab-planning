﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;

namespace HM2.BLL.IServices
{
   public interface IUser
    {
        UserModel checkUser(string username, string password);
        bool registerUser(UserModel userModel, StudentModel student);
        string addUser(UserModel lab);
    }
}
