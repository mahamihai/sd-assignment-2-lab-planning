﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;

namespace HM2.BLL.IServices
{
  public  interface ILab
    {
        List<LaboratoryModel> getLaboratories();
        string addLaboratory(LaboratoryModel lab);
        string updateLaboratory(LaboratoryModel lab);
        string deleteLab(object id);
        List<LaboratoryModel> getLabsFiltered(string filter);
    }
}
